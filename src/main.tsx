import React from 'react';
import ReactDOM from 'react-dom';
import { GraphDomain } from 'react-graph-state';
import App from './App';

ReactDOM.render(
	<React.StrictMode>
		<GraphDomain>
			<App />
		</GraphDomain>
	</React.StrictMode>,
	document.getElementById('root')
);
