import React from 'react';
import { Route, Router, Switch } from 'react-router-dom';
import Template from './Components/Main/Template';
import Login from './Pages/Login/Login';
import history from './Utilities/history';
function App () {
	if (localStorage.username) {
		return (
			<Router history={history}>
				<Switch>
					<Route>
						<Template />
					</Route>
				</Switch>
			</Router>
		);
	}

	return <Login />;
}

export default App;
