import React from 'react';
import Card from '../../Components/Card/Card';
import history from '../../Utilities/history';

const TodoList = () => {
	const handleClick = () => {
		history.push('/todo-list/create');
	};

	return (
		<Card title='Todo List'>
			<button className='btn btn-success' onClick={handleClick}>
				<i className='fa fa-plus' /> Add Todo
			</button>
		</Card>
	);
};

export default TodoList;
