import React from 'react';
import { useGraphNodeDispatch } from 'react-graph-state';
import InputText from '../../../Components/Inputs/InputText';
import { IInvoiceItem, InvoiceEditItemNode, InvoiceRemoveItemNode } from '../../../Nodes/InvoiceNode';

interface Props {
	index: number;
	row: IInvoiceItem;
}

const TodoAddEditFormItem: React.FC<Props> = ({ index, row }) => {
	const updateTodo = useGraphNodeDispatch(InvoiceEditItemNode);
	const removeTodo = useGraphNodeDispatch(InvoiceRemoveItemNode);

	const handleChange = (e: any) => {
		const { name, value } = e.target;

		updateTodo({ index, value, name });
	};

	const handleClickRemoveTodo = () => {
		removeTodo({ index, name: '', value: '' });
	};

	return (
		<tr>
			<td>{index + 1}</td>
			<td>
				<InputText name='item_description' handleChange={handleChange} data={row} />
			</td>
			<td>
				<InputText name='quantity' handleChange={handleChange} data={row} type='number' />
			</td>
			<td>
				<InputText name='price' handleChange={handleChange} data={row} type='number' />
			</td>
			<td className='pt-3'>{row.total}</td>
			<td>
				<button className='btn btn-danger' onClick={handleClickRemoveTodo}>
					<i className='fa fa-trash' />
				</button>
			</td>
		</tr>
	);
};
export default TodoAddEditFormItem;
