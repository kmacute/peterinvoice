import React from 'react';
import { useGraphNodeDispatch } from 'react-graph-state';
import { InvoiceAddItemNode } from '../../../Nodes/InvoiceNode';

const TodoAddEditFormAddButton = () => {
	const add = useGraphNodeDispatch(InvoiceAddItemNode);

	const handleClick = () => {
		add();
	};

	return (
		<button className='btn btn-success mb-2' onClick={handleClick}>
			<i className='fa fa-plus me-1'> </i> Add
		</button>
	);
};

export default TodoAddEditFormAddButton;
