import React from 'react';
import { useGraphNodeState } from 'react-graph-state';
import BackButton from '../../Components/Button/BackButton';
import Card from '../../Components/Card/Card';
import InputText from '../../Components/Inputs/InputText';
import { InvoiceNode } from '../../Nodes/InvoiceNode';
import TodoAddEditFormAddButton from './components/TodoAddEditFormAddButton';
import TodoAddEditFormItem from './components/TodoAddEditFormItem';

const TodoAddEdit = () => {
	return (
		<Card title='Add Todo'>
			<BackButton />
			<hr />
			<TodoAddEditForm />
		</Card>
	);
};

const TodoAddEditForm = () => {
	const [ data, setData ] = useGraphNodeState(InvoiceNode);

	const handleChange = (e: any) => {
		const { name, value } = e.target;

		setData((prev) => ({
			...prev,
			[name]: value
		}));
	};

	return (
		<div>
			<InputText name='customer_name' label='Customer Name' handleChange={handleChange} data={data} />
			<InputText name='discount_percent' label='Discount %' handleChange={handleChange} data={data} type='number' />
			<div className='mb-3'>
				Total Amount: <strong>{data.total_amount}</strong>
			</div>
			<Card title='Todo Items'>
				<TodoAddEditFormAddButton />

				<table className='table'>
					<thead>
						<tr>
							<th style={{ width: '0.001%' }}>#</th>
							<th>Description</th>
							<th style={{ width: '150px' }}>Quantity</th>
							<th style={{ width: '150px' }}>Price</th>
							<th style={{ width: '150px' }}>Total</th>
						</tr>
					</thead>
					<tbody>{data.details.map((row, key) => <TodoAddEditFormItem key={key} index={key} row={row} />)}</tbody>
				</table>
			</Card>
		</div>
	);
};

export default TodoAddEdit;
