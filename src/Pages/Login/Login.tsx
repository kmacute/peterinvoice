import React from 'react';
import { useGraphNodeDispatch, useGraphNodeState } from 'react-graph-state';
import { loginNode, loginSubmit } from '../../Nodes/LoginNode';
import './Login.css';

const Login = () => {
	const [ data, setData ] = useGraphNodeState(loginNode);
	const submit = useGraphNodeDispatch(loginSubmit);

	const handleChange = (e: any) => {
		const { name, value } = e.target;

		setData((prev) => ({
			...prev,
			[name]: value
		}));
	};

	const handleSubmit: React.FormEventHandler<HTMLFormElement> = async (e) => {
		e.preventDefault();

		await submit();
	};

	return (
		<div className='form-signin-body text-center'>
			<main className='form-signin'>
				<form onSubmit={handleSubmit}>
					<i className='fa fa-user-circle-o fa-4x text-primary' />
					<h1 className='h3 mb-3 fw-normal'>Please sign in</h1>
					<div className='form-floating'>
						<input
							type='text'
							className='form-control'
							id='floatingInput'
							placeholder='username'
							name='username'
							value={data.username}
							onChange={handleChange}
						/>
						<label htmlFor='floatingInput'>Username</label>
					</div>
					<div className='form-floating'>
						<input
							type='password'
							className='form-control'
							id='floatingPassword'
							placeholder='Password'
							name='password'
							value={data.password}
							onChange={handleChange}
						/>
						<label htmlFor='floatingPassword'>Password</label>
					</div>
					<div className='checkbox mb-3'>
						<label>
							<input type='checkbox' defaultValue='remember-me' /> Remember me
						</label>
					</div>
					<button className='w-100 btn btn-lg btn-primary' type='submit'>
						Sign in
					</button>
					<p className='mt-5 mb-3 text-muted'>© 2017–2021</p>
				</form>
			</main>
		</div>
	);
};

export default Login;
