import React from 'react';

interface Props {
	title: string;
}

const Card: React.FC<Props> = ({ title, children }) => {
	return (
		<div className='card'>
			<div className='card-header'>
				<strong>{title}</strong>
			</div>
			<div className='card-body'>{children}</div>
		</div>
	);
};

export default Card;
