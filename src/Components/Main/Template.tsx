import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Home from '../../Pages/Home/Home';
import TodoAddEdit from '../../Pages/TodoList/TodoAddEdit';
import TodoList from '../../Pages/TodoList/TodoList';
import Navs from './components/Navs';

const Template = () => {
	return (
		<div>
			<Navs />

			<div className='container-fluid mt-2'>
				<Switch>
					<Route path='/' exact>
						<Home />
					</Route>
					<Route path='/todo-list' exact>
						<TodoList />
					</Route>
					<Route path='/todo-list/create' exact>
						<TodoAddEdit />
					</Route>
				</Switch>
			</div>
		</div>
	);
};

export default Template;
