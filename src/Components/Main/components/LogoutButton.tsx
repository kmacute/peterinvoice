import React from 'react';

const LogoutButton = () => {
	const handleClick = () => {
		localStorage.clear();
		location.href = '/';
	};
	return (
		<li className='nav-item'>
			<a className='nav-link' href='#' onClick={handleClick}>
				Logout
			</a>
		</li>
	);
};

export default LogoutButton;
