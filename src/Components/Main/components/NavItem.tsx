import React from 'react';
import { NavLink } from 'react-router-dom';

interface Props {
	label: string;
	url: string;
}
const NavItem: React.FC<Props> = ({ label, url }) => {
	return (
		<li className='nav-item'>
			<NavLink className='nav-link' to={url} exact>
				{label}
			</NavLink>
		</li>
	);
};

export default NavItem;
