import React from 'react';

interface Props {
	label?: string;
	name: string;
	handleChange: any;
	data: any[any];
	type?: string;
}

const InputText: React.FC<Props> = ({ label, name, handleChange, data, type = 'text' }) => {
	return (
		<div className=''>
			{label && (
				<label htmlFor='' className='form-label'>
					{label}
				</label>
			)}

			<input type={type} className='form-control' name={name} onChange={handleChange} value={data[name]} />
		</div>
	);
};

export default InputText;
