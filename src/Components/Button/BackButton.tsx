import React from 'react';
import history from '../../Utilities/history';

const BackButton = () => {
	const handleClick = () => {
		history.goBack();
	};

	return (
		<button className='btn btn-warning' onClick={handleClick}>
			<i className='fa fa-arrow-left' /> Back
		</button>
	);
};

export default BackButton;
