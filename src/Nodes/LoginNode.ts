import { createGraphNode } from 'graph-state';

interface ILogin {
	username: string;
	password: string;
}

export const loginNode = createGraphNode<ILogin>({
	key: 'loginNode',
	get: {
		username: '',
		password: ''
	}
});

export const loginSubmit = createGraphNode<undefined, void>({
	key: 'loginSubmit',
	get: undefined,
	set: async ({ get }) => {
		const data = get(loginNode);

		if (data.username == '' || data.password == '') {
			alert('Username or password is required');
			return;
		}

		if (!(data.username == 'admin' && data.password == 'admin')) {
			alert('Invalid username or password');
			return;
		}

		localStorage.username = 'admin';
		location.href = '/';
	}
});
