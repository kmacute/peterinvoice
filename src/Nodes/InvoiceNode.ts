import { createGraphNode } from 'graph-state';

export interface IInvoice {
	customer_name: string;
	sub_total: number;
	total_amount: number;
	discount_percent: number;
	details: IInvoiceItem[];
}

export interface IInvoiceItem {
	item_description: string;
	quantity: number;
	price: number;
	total: number;
}

export interface IEditItem {
	index: number;
	name: string;
	value?: string;
}

export const InvoiceNode = createGraphNode<IInvoice>({
	key: 'InvoiceNode',
	get: {
		customer_name: '',
		sub_total: 0,
		total_amount: 0,
		discount_percent: 0,
		details: []
	}
});

export const InvoiceAddItemNode = createGraphNode<undefined, void>({
	key: 'InvoiceAddItemNode',
	get: undefined,
	set: async ({ get, set }) => {
		const data = get(InvoiceNode);

		const item = {
			item_description: 'Change THis item description',
			price: 100,
			quantity: 1,
			total: 100
		};

		// shortcut
		const newData = {
			...data,
			details: [ ...data.details, item ]
		};

		set(InvoiceNode, newData);
	}
});

export const InvoiceEditItemNode = createGraphNode<undefined, IEditItem>({
	key: 'InvoiceEditItemNode',
	get: undefined,
	set: async ({ get, set }, param) => {
		const data = get(InvoiceNode);

		// 1. Update Details
		// only specific index or row
		const newDetails = data.details.map((item: IInvoiceItem, key: number) => {
			// Skip unwanted item
			if (key != param.index) {
				return item;
			}

			// create new item with updated property
			let newItem = {
				...item,
				[param.name]: param.value
			};

			// optional
			// calculate row total
			if ([ 'quantity', 'price' ].includes(param.name)) {
				newItem = {
					...newItem,
					total: newItem.quantity * newItem.price
				};
			}

			return newItem;
		});

		// 2. Calculate New Sub Total
		// reduce((runningTotal, currentRowLooped) => runningTotal + currentRowLooped, initialValueOfRunninTotal)
		const subTotal = newDetails.reduce((total, row) => total + row.total, 0);

		// 3. Calculate Discount by Percentage
		// discount_percentage = 20
		const discountPercent = (100 - data.discount_percent) / 100;
		const grandTotal = subTotal * discountPercent;

		// 4. Construct newData and apply new values
		const newData = {
			...data, // explode data
			details: newDetails,
			sub_total: subTotal,
			total_amount: grandTotal
		};

		// 5. Apply changes
		set(InvoiceNode, newData);
	}
});

export const InvoiceRemoveItemNode = createGraphNode<undefined, IEditItem>({
	key: 'InvoiceRemoveItemNode',
	get: undefined,
	set: async ({ get, set }, param) => {
		const data = get(InvoiceNode);

		// get all list where index is not param.index
		const newDetails = data.details.filter((_, index) => index != param.index);

		// shortcut
		const newData = {
			...data,
			details: newDetails
		};

		set(InvoiceNode, newData);
	}
});
